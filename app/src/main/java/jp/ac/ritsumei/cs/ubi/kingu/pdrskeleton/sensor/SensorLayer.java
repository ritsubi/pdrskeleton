/*
 * Copyright (C) 2008-2015 Ritsumeikan University Nishio Laboratory All Rights Reserved.
 */

package jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton.sensor;

import java.util.ArrayList;

import jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton.sensor.object.Accelerometer;
import jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton.sensor.object.Gyro;

abstract public class SensorLayer {
    protected ArrayList<SensorInterface> sensorInterfaces = new ArrayList<>();

    public void addListener(SensorInterface sensorInterface) {
        sensorInterfaces.add(sensorInterface);
    }

    public void removeListener(SensorInterface sensorInterface) {
        sensorInterfaces.remove(sensorInterface);
    }

    abstract public void startSensor();

    abstract public void stopSensor();

    protected void onAccelerometerChanged(Accelerometer accelerometer) {
        for (SensorInterface sensorInterface : sensorInterfaces) {
            sensorInterface.onAccelerometerChanged(accelerometer);
        }
    }

    protected void onGyroChanged(Gyro gyro) {
        for (SensorInterface sensorInterface : sensorInterfaces) {
            sensorInterface.onGyroChanged(gyro);
        }
    }
}

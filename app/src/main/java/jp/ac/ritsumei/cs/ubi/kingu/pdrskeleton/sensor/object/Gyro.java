/*
 * Copyright (C) 2008-2015 Ritsumeikan University Nishio Laboratory All Rights Reserved.
 */

package jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton.sensor.object;

public class Gyro {
    private long time;
    private double timestampSeconds;
    private float x;
    private float y;
    private float z;

    public Gyro(long time, float x, float y, float z) {
        this.time = time;
        this.timestampSeconds = time * (1e-9);
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public long getTime() {
        return time;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    @Override
    public String toString() {
        return timestampSeconds + "," + x + "," + y + "," + z;
    }
}

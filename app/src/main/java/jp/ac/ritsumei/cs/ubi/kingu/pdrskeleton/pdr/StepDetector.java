/*
 * Copyright (C) 2008-2015 Ritsumeikan University Nishio Laboratory All Rights Reserved.
 */
package jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton.pdr;

import java.util.ArrayList;

public class StepDetector {
    /**
     * These listeners are called when this class detect a step.
     */
    protected ArrayList<StepListener> mStepListeners = new ArrayList<>();

    /**
     * Registers a StepListener for the Leave Event.
     *
     * @param stepListener A StepListener object.
     */
    public void addListener(StepListener stepListener) {
        mStepListeners.add(stepListener);
    }


    /**
     * Returns if this Collection contains no elements. This implementation tests, whether size returns 0.
     *
     * @return if there is no listeners, return true.
     */
    public boolean isEmpty() {
        return mStepListeners.isEmpty();
    }
}
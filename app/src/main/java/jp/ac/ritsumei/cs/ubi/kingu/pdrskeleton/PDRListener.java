/*
 * Copyright (C) 2008-2015 Ritsumeikan University Nishio Laboratory All Rights Reserved.
 */

package jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton;

public interface PDRListener {
    void onPDRChanged(double latitude, double longitude, double directionDegree);
}
